#ifndef WOODEN_GAME_WOODEN_DEBUG_H
#define WOODEN_GAME_WOODEN_DEBUG_H

void dprint(const std::string &msg, bool need_endl = true);
void dprint(const char *msg, bool need_endl = true);

#endif //WOODEN_GAME_WOODEN_DEBUG_H
